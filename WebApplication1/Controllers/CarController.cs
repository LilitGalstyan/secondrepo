﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarController : ControllerBase
    {
        private static readonly string[] Cars = new[]
       {
            "Mercedes", "BMW", "Chevrolette", "Toyota", "Audi"
        };
        private static readonly string[] Colours = new[]
       {
            "Black", "White", "Green", "Yelloww", 
        };
        private readonly ILogger<CarController> _logger;
        public CarController(ILogger<CarController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public IEnumerable<Cars> Get()
        {
            var rng = new Random();
            return (IEnumerable<Cars>)Enumerable.Range(1, 5).Select(index => new Cars
            {
                CarModel = Cars[rng.Next(Cars.Length)],
                CarYear= rng.Next(2001,2021),
                CarColour=Colours[rng.Next(Colours.Length)],

            })
            .ToArray();
        }



    }
}
